# samba

Review docker-compose.yml environment/hostname if needed

- hostname and envs with hostname: set it to your actual hostname
- "SAMBA_HOSTS_ALLOW=0.0.0.0/0"
- Bind volume inside docker is /samba

Download your isos (see scripts in isos folder)

Mount samba in IPMI Virtual Media. For example: `/isos/ubuntu-22.04.3-live-server-amd64.iso`

![docs/mount.png](docs/mount.png)

Reboot server from IPMI and F11 to select boot. Virtual media should be now a boot option.

![docs/boot.png](docs/boot.png)

# Test from Linux

	apt install smbclient cifs-utils
	mount -t cifs //172.31.4.101/isos /mnt

# Credits

From https://github.com/crazy-max/docker-samba
